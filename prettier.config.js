module.exports = {
    printWidth: 130,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'es5', // other options `none` or `es5` or `all`
    bracketSpacing: true,
    arrowParens: 'avoid', // other option 'always'
};
