# Ibotta Anagrams Server

## How to run it

### Online

The server is install on heroku at https://ibotta-anagram-node.herokuapp.com/

### Or install it

Install it by running git clone https://gitlab.com/lidholm/ibotta-anagram-node-server.git and then run npm install
Start the server by running npm run start

## API

To get all anagrams for a word

_GET /anagrams/:word.json_

To add new words

_POST /words.json with {"words" => ["read", "dear", "dare", "Ared"] } as body_

To delete all word

_DELETE/words.json_

To delete one word

_DELETE/words/:word.json_

To delete one word and all its anagrams

_DELETE/anagrams/:word.json_

To get minimum and maximum length of a word, the average and median length of a word

_GET /countWords/_

To get a list of the word with most anagrams

_GET /mostAnagrams/_

To get all words with anagrams of at least minSize

_GET /anagramsOfSize/:minSize_

To check if all the words supplied are anagrams of each other

_POST /allAnagrams/ with {"words" => ["read", "dear", "dare", "Ared"] } as body_

## Features that I think would be useful to add

It could be interesting to have an endpoint where you send in a word and it returns a list of words that together makes an anagram of the word sent in.

## Implementation details

I decided to try and store all the data in memory, since that would make it easier and was allowed.

To store the words, my approach is to get a wordKey that is all the letters of the word sorted alphabetically. This is done since it is easy to compare if two words are anagrams. If they have the same wordKey they are anagrams, i.e dare and dear would both have the same wordKey: ader.
So I decided to store all words in a hashmap where the key will be the wordKey and the value of the hashmap is a list of words with that wordKey.
This makes it both quick to store the word as well as getting the words. Arguably I could have stored the list of words in another hashmap to make that slightly faster to check for words in that list and remove them, but since there aren’t too many words for one anagram that isn’t a problem.

Checking for the longest, shortest word, as well as for the median and average word length using the data structure above would make it quite time consuming since I would then need to loop over all wordKeys and then loop over the list of words.
So I instead decided on storing the data for this in another data structure, a hashmap where the key is the length of the word and the value is the number of words of that length. When adding a word or deleting a word from the API I would update this hashmap to reflect that.
This makes it fast to find the shortest and longest, since that can be found by just checking the keys of the hashmap. For the average word length I would find out the total length of all words by multiplying each key by the number of words for it and summarize it and then divide by number of words. Getting the median is done by creating a sorted list where each item is the length of a word and then getting the number in the middle (or 2 numbers in the middle / 2). This part is slightly inefficient but didn’t seem to be too bad for the number of words in the dictionary and so I let that implementation stay the way it is.

Lastly, for getting the anagram groups of a certain size I did something similar to the above. I have a hashmap where the key is the number of anagrams and the value is another hashmap of the wordKeys as keys . I then need to update the hashmap whenever a word is added or deleted in the API.
It makes it fast to find the anagram groups of a certain size or the words with the most anagrams.

It is convenient that I could fit all of the data in a memory cache, but using a key-value database to store it in using the keys of the hashmaps as keys to the database should have made it pretty fast still. Adding the entire dictionary could potentially be significantly slower.

## Limitations

I currently set a limit on the max number of anagrams for a word to be 40, but that could easily be changed to be dynamic. I didn’t change it since all the words in the dictionary fit.
I haven’t seen any other limitations on either lengths of words or number of words. Obviously by adding more and more words the server would run out of memory, but it should be relatively easy to switch to using a key-value database

## Design overview and trade-offs

I see the API mostly as an API you would query to get anagrams out of and occasionally add or remove a word from.
Putting everything in hashmaps works pretty well for this use case, but it’s limiting in other ways and can limit the ability to easily extend the API with a whole lot of new functionality. For example, trying to get all words from the dictionary starting the letter “T” would not be efficient, since you would practically have to loop over all words.
