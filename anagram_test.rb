#!/usr/bin/env ruby

require 'json'
require_relative 'anagram_client'
require 'test/unit'

# capture ARGV before TestUnit Autorunner clobbers it

class TestCases < Test::Unit::TestCase

  def setup
    @client = AnagramClient.new(ARGV)

    @client.post('/words.json', nil, {"words" => ["read", "dear", "dare", "Ared"] }) rescue nil
  end

  def teardown
    @client.delete('/words.json') rescue nil
  end

  def test_adding_words
    res = @client.post('/words.json', nil, {"words" => ["read", "dear", "dare"] })

    assert_equal('201', res.code, "Unexpected response code")
  end

  def test_fetching_anagrams
    res = @client.get('/anagrams/read.json')

    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)

    body = JSON.parse(res.body)

    assert_not_nil(body['anagrams'])

    expected_anagrams = %w(dare dear)
    assert_equal(expected_anagrams, body['anagrams'].sort)
  end

  def test_fetching_anagrams_with_limit
    res = @client.get('/anagrams/read.json', 'limit=1')

    assert_equal('200', res.code, "Unexpected response code")

    body = JSON.parse(res.body)

    assert_equal(1, body['anagrams'].size)
  end

  def test_fetch_for_word_with_no_anagrams
    res = @client.get('/anagrams/ared.json')

    assert_equal('200', res.code, "Unexpected response code")

    body = JSON.parse(res.body)

    assert_equal(0, body['anagrams'].size)
  end

  def test_deleting_all_words
    res = @client.delete('/words.json')

    assert_equal('204', res.code, "Unexpected response code")

    # should fetch an empty body
    res = @client.get('/anagrams/read.json')

    assert_equal('200', res.code, "Unexpected response code")

    body = JSON.parse(res.body)

    assert_equal(0, body['anagrams'].size)
  end

  def test_deleting_all_words_multiple_times
    3.times do
      res = @client.delete('/words.json')

      assert_equal('204', res.code, "Unexpected response code")
    end

    # should fetch an empty body
    res = @client.get('/anagrams/read.json', 'limit=1')

    assert_equal('200', res.code, "Unexpected response code")

    body = JSON.parse(res.body)

    assert_equal(0, body['anagrams'].size)
  end

  def test_deleting_single_word
    # delete the word
    res = @client.delete('/words/dear.json')

    assert_equal('204', res.code, "Unexpected response code")

    # expect it not to show up in results
    res = @client.get('/anagrams/read.json')

    assert_equal('200', res.code, "Unexpected response code")

    body = JSON.parse(res.body)

    assert_equal(['dare'], body['anagrams'])
  end

  def test_count_words
    @client.post('/words.json', nil, {"words" => ["percussion", "supersonic"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["art", "rat","tar"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["are", "ear"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["nameless", "salesmen"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil

    # Adding words again should not have an effect
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil


    res = @client.get('/countWords')

    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)

    body = JSON.parse(res.body)

    assert_not_nil(body['count'])
    assert_equal(15, body['count'])
    assert_equal(2, body['min'])
    assert_equal(10, body['max'])
    assert_equal(4.733333333333333, body['average'])
    assert_equal(4, body['median'])


    res = @client.delete('/words/no.json')
    res = @client.delete('/words/on.json')
    res = @client.delete('/words/percussion.json')
    res = @client.delete('/words/supersonic.json')
    res = @client.delete('/words/dear.json')
    res = @client.delete('/words/missing.json')


    res = @client.get('/countWords')

    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)

    body = JSON.parse(res.body)

    assert_not_nil(body['count'])

    assert_equal(10, body['count'])
    assert_equal(3, body['min'])
    assert_equal(8, body['max'])
    assert_equal(4.3, body['average'])
    assert_equal(3.5, body['median'])
  end


  def test_number_of_anagrams
    res = @client.delete('/words/Ared.json')

    @client.post('/words.json', nil, {"words" => ["percussion", "supersonic"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["art", "rat","tar"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["are", "ear"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["nameless", "salesmen"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil

  #   # Adding words again should not have an effect
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil

    res = @client.get('/mostAnagrams')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["read","dear","dare"],["art","rat","tar"]], body['anagrams'])


    res = @client.delete('/words/dear.json')
    res = @client.get('/mostAnagrams')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["art","rat","tar"]], body['anagrams'])


    @client.post('/words.json', nil, {"words" => ["rade"] }) rescue nil
    res = @client.get('/mostAnagrams')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["art","rat","tar"],["read","dare","rade"]], body['anagrams'])
  end

  def test_anagram_groups_of_min_size
    res = @client.delete('/words/Ared.json')

    @client.post('/words.json', nil, {"words" => ["percussion", "supersonic"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["art", "rat","tar"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["are", "ear"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["nameless", "salesmen"] }) rescue nil
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil

    # Adding words again should not have an effect
    @client.post('/words.json', nil, {"words" => ["no", "on"] }) rescue nil

    res = @client.get('/anagramsOfSize/3')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["read","dear","dare"],["art","rat","tar"]], body['anagrams'])


    res = @client.get('/anagramsOfSize/2')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["read","dear","dare"],["art","rat","tar"],["percussion","supersonic"],["are","ear"],["nameless","salesmen"],["no","on"]], body['anagrams'])



    res = @client.delete('/words/dear.json')
    res = @client.get('/anagramsOfSize/3')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["art","rat","tar"]], body['anagrams'])


    @client.post('/words.json', nil, {"words" => ["rade"] }) rescue nil
    res = @client.get('/anagramsOfSize/3')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["art","rat","tar"],["read","dare","rade"]], body['anagrams'])
  end

  def test_if_all_words_are_anagrams
    res = @client.post('/allAnagrams', nil, {"words" => ["read", "dear", "dare"] })
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['all'])
    assert_equal(true, body['all'])

    res = @client.post('/allAnagrams', nil, {"words" => ["read", "dear", "some"] })
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['all'])
    assert_equal(false, body['all'])
  end

  def test_deleting_word_and_all_of_its_anagrams
    res = @client.delete('/words/Ared.json')

    @client.post('/words.json', nil, {"words" => ["art", "rat","tar"] }) rescue nil
    res = @client.get('/anagrams/rat.json')
    assert_equal('200', res.code, "Unexpected response code")
    body = JSON.parse(res.body)
    assert_equal(['art',"tar"], body['anagrams'])

    res = @client.get('/anagramsOfSize/2')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["read","dear","dare"],["art","rat","tar"]], body['anagrams'])

    res = @client.get('/countWords')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['count'])
    assert_equal(6, body['count'])
    assert_equal(3, body['min'])
    assert_equal(4, body['max'])
    assert_equal(3.5, body['average'])
    assert_equal(3.5, body['median'])




    res = @client.delete('/anagrams/art.json')
    assert_equal('204', res.code, "Unexpected response code")
    res = @client.get('/anagrams/rat.json')
    assert_equal('200', res.code, "Unexpected response code")
    body = JSON.parse(res.body)
    assert_equal([], body['anagrams'])

    res = @client.get('/anagramsOfSize/2')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["read","dear","dare"]], body['anagrams'])

    res = @client.get('/countWords')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['count'])
    assert_equal(3, body['count'])
    assert_equal(4, body['min'])
    assert_equal(4, body['max'])
    assert_equal(4, body['average'])
    assert_equal(4, body['median'])


    @client.post('/words.json', nil, {"words" => ["acres", "cares","races","scare"] }) rescue nil
    res = @client.get('/anagrams/cares.json')
    assert_equal('200', res.code, "Unexpected response code")
    body = JSON.parse(res.body)
    assert_equal(['acres',"races","scare"], body['anagrams'])

    res = @client.get('/anagramsOfSize/2')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    assert_equal([["acres","cares","races","scare"],["read","dear","dare"]], body['anagrams'])

    res = @client.get('/countWords')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['count'])
    assert_equal(7, body['count'])
    assert_equal(4, body['min'])
    assert_equal(5, body['max'])
    assert_equal(4.571428571428571, body['average'])
    assert_equal(5, body['median'])
  end

  def test_fetching_anagrams_with_proper_nouns_param
    # Actually checking for case insensitivity, but will work for proper nouns too
    res = @client.delete('/words/dare.json')
    @client.post('/words.json', nil, {"words" => ["Ared", "dAre"] }) rescue nil

    res = @client.get('/anagrams/read.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(dear)
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/read.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(Ared dAre dear)
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w()
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(dAre dear read)
    assert_equal(expected_anagrams, body['anagrams'].sort)


    res = @client.delete('/words/dear.json')

    res = @client.get('/anagrams/read.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w()
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/read.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(Ared dAre)
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w()
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(dAre read)
    assert_equal(expected_anagrams, body['anagrams'].sort)


    @client.post('/words.json', nil, {"words" => ["dear"] }) rescue nil
    res = @client.delete('/words/dAre.json')

    res = @client.get('/anagrams/read.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(dear)
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/read.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(Ared dear)
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=false')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w()
    assert_equal(expected_anagrams, body['anagrams'].sort)

    res = @client.get('/anagrams/Ared.json', 'propernouns=true')
    assert_equal('200', res.code, "Unexpected response code")
    assert_not_nil(res.body)
    body = JSON.parse(res.body)
    assert_not_nil(body['anagrams'])
    expected_anagrams = %w(dear read)
    assert_equal(expected_anagrams, body['anagrams'].sort)
  end

  def checkEmptyResponse(res, code)
    assert_equal(code, res.code, "Unexpected response code")
  end

  def checkResponse(res, code, data)
    assert_equal(code, res.code, "Unexpected response code")
    body = JSON.parse(res.body)
    assert_equal(data, body)
  end

  def test_sending_faulty_or_empty_data
    res = @client.delete('/words/notaword.json')
    checkEmptyResponse(res, '204')

    res = @client.post('/words.json', nil, {"words" => [] }) rescue nil
    checkEmptyResponse(res, '201')

    res = @client.get('/anagrams/rat.json')
    checkResponse(res, '200', {"anagrams" => []})

    res = @client.delete('/anagrams/notaword.json')
    checkEmptyResponse(res, '204')

    res = @client.get('/mostAnagrams/')
    checkResponse(res, '200', {"anagrams" => [["read", "dear", "dare", "Ared"]]})

    res = @client.get('/countWords')
    checkResponse(res, '200', {'count' => 4, "min" => 4, "max" => 4, "average" => 4, "median" => 4})

    res = @client.get('/anagramsOfSize/3')
    checkResponse(res, '200', {"anagrams" => [["read", "dear", "dare", "Ared"]]})

    res = @client.get('/anagramsOfSize/5')
    checkResponse(res, '200', {"anagrams" => []})


    res = @client.post('/allAnagrams', nil, {"words" => ['abby','baby'] }) rescue nil
    checkResponse(res, '200', {"all" => true})


    res = @client.post('/allAnagrams', nil, {"words" => ['some','else'] }) rescue nil
    checkResponse(res, '200', {"all" => false})

  end

end
