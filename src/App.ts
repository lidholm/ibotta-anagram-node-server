import * as express from 'express';
import { ExpirationStrategy, MemoryStorage } from 'node-ts-cache';
import * as bodyParser from 'body-parser';
import fs = require('fs');

const myCache = new ExpirationStrategy(new MemoryStorage());

export class NumberOfAnagrams {
    public lengths: object[];
    public MAX_LENGTH = 40;

    constructor() {
        this.lengths = [];
        for (let i = 0; i < this.MAX_LENGTH; i++) {
            this.lengths.push({});
        }
    }

    public addWord(wordKey, numberOfWords) {
        this.lengths[numberOfWords][wordKey] = true;
        delete this.lengths[numberOfWords - 1][wordKey];
    }

    public deleteWord(wordKey, numberOfWords) {
        this.lengths[numberOfWords][wordKey] = true;
        delete this.lengths[numberOfWords + 1][wordKey];
    }

    getMostAnagrams(): any {
        for (let i = this.MAX_LENGTH - 1; i >= 0; i--) {
            if (Object.keys(this.lengths[i]).length > 0) {
                return Object.keys(this.lengths[i]);
            }
        }
        return [];
    }

    getAnagramsOfMinimunSize(minSize) {
        let wordKeys = [];

        for (let i = this.MAX_LENGTH - 1; i >= minSize; i--) {
            if (Object.keys(this.lengths[i]).length > 0) {
                wordKeys = wordKeys.concat(Object.keys(this.lengths[i]));
            }
        }
        return wordKeys;
    }
}

export class App {
    public express;
    private WORD_KEY = 'WORD_KEY';
    private WORD_LENGTH_KEY = 'WORD_LENGTH_KEY';
    private NUMBER_OF_ANAGRAMS = 'NUMBER_OF_ANAGRAMS';
    private dataStore = {};
    private wordLengths = {};
    private numberOfAnagrams = new NumberOfAnagrams();

    constructor() {
        this.setup();
    }

    protected setup() {
        this.express = express();
        this.config();
        this.mountRoutes();
        this.loadData();
    }

    private config(): void {
        this.express.use(bodyParser.json());
    }

    async loadData() {
        this.deleteAll();
        const data = fs.readFileSync('dictionary.txt', 'utf8');
        const words = data.split('\n');
        await this.addWords(words);
    }

    getWordKey(word: string, caseSensitive: boolean = false) {
        if (!caseSensitive) {
            word = word.toLowerCase();
        }
        return word
            .split('')
            .sort()
            .join('');
    }

    unique(list: any[]) {
        return list.filter((a, i) => {
            return list.indexOf(a) == i;
        });
    }

    async saveDatastore() {
        await myCache.setItem(this.WORD_KEY, this.dataStore, { isCachedForever: true });
        await myCache.setItem(this.WORD_LENGTH_KEY, this.wordLengths, { isCachedForever: true });
        await myCache.setItem(this.NUMBER_OF_ANAGRAMS, this.numberOfAnagrams, { isCachedForever: true });
    }

    async loadDatastore() {
        this.dataStore = await myCache.getItem<any>(this.WORD_KEY);
        if (this.dataStore === undefined) {
            this.dataStore = {};
        }

        this.wordLengths = await myCache.getItem<any>(this.WORD_LENGTH_KEY);
        if (this.wordLengths === undefined) {
            this.wordLengths = {};
        }

        this.numberOfAnagrams = await myCache.getItem<NumberOfAnagrams>(this.NUMBER_OF_ANAGRAMS);
        if (this.numberOfAnagrams === undefined) {
            this.numberOfAnagrams = new NumberOfAnagrams();
        }
    }

    getWordsForWordKeys(wordKey: string, caseSensitive: boolean = true) {
        let words = this.dataStore[wordKey];
        if (words === undefined) {
            words = [];
        }
        if (caseSensitive) {
            words = words.filter(word => {
                return this.getWordKey(word, true) === wordKey;
            });
        }
        return words;
    }

    setWordsForWordKeys(wordKey: string, words: string[]) {
        this.dataStore[wordKey] = words;
    }

    removeWordFromArray(array: string[], word: string) {
        const newArray = array.filter(x => x !== word);
        return newArray;
    }

    addToWordLengths(word: string) {
        if (!(word.length in this.wordLengths)) {
            this.wordLengths[word.length] = 0;
        }
        this.wordLengths[word.length]++;
    }

    deleteFromWordLengths(word: string) {
        this.wordLengths[word.length]--;
        if (this.wordLengths[word.length] <= 0) {
            delete this.wordLengths[word.length + ''];
        }
    }

    addWordToNumberOfAnagramDict(wordKey: string, numberOfWords: number) {
        this.numberOfAnagrams.addWord(wordKey, numberOfWords);
    }

    deleteWordFromNumberOfAnagramDict(wordKey: string, numberOfWords: number) {
        this.numberOfAnagrams.deleteWord(wordKey, numberOfWords);
    }

    // API
    async getAnagrams(word: string, caseSensitive = true) {
        await this.loadDatastore();

        const wordKey = this.getWordKey(word);
        let wordsForWordKey = this.getWordsForWordKeys(wordKey, caseSensitive);
        wordsForWordKey = wordsForWordKey.slice(0);
        if (wordsForWordKey === undefined) {
            wordsForWordKey = [];
        }
        return wordsForWordKey;
    }

    checkIfWordExistsInList(word: string, list: any[]) {
        return list.includes(word);
    }

    getLimitedItemsFromList(list: any[], limit: number) {
        if (limit === undefined) {
            return list;
        }
        return list.slice(0, limit);
    }

    async addWords(words: string[]) {
        await this.loadDatastore();

        for (let i = 0; i < words.length; i++) {
            await this.addWord(words[i], false);
        }

        await this.saveDatastore();
    }

    async addWord(word: string, save: boolean = true) {
        if (word.length === 0) return;

        if (save) {
            await this.loadDatastore();
        }

        const wordKey = this.getWordKey(word);
        let wordsForWordKey = await this.getWordsForWordKeys(wordKey, false);

        if (wordsForWordKey.includes(word)) {
            return;
        }
        wordsForWordKey.push(word);

        this.setWordsForWordKeys(wordKey, wordsForWordKey);

        this.addToWordLengths(word);
        this.addWordToNumberOfAnagramDict(wordKey, wordsForWordKey.length);

        if (save) {
            await this.saveDatastore();
        }
    }

    async deleteWord(word: string) {
        await this.loadDatastore();

        const wordKey = this.getWordKey(word);
        let wordsForWordKey = await this.getWordsForWordKeys(wordKey, false);

        if (wordsForWordKey.includes(word)) {
            wordsForWordKey = this.removeWordFromArray(wordsForWordKey, word);
            this.setWordsForWordKeys(wordKey, wordsForWordKey);
            this.deleteFromWordLengths(word);
            this.deleteWordFromNumberOfAnagramDict(wordKey, parseInt(wordsForWordKey.length));
            await this.saveDatastore();
        }
    }

    async deleteAll() {
        this.loadDatastore();
        this.dataStore = {};
        this.wordLengths = {};
        this.numberOfAnagrams = new NumberOfAnagrams();
        this.saveDatastore();
    }

    async deleteWordAndAnagrams(word: string) {
        await this.loadDatastore();

        const wordKey = this.getWordKey(word);
        let wordsForWordKey = await this.getWordsForWordKeys(wordKey);
        delete this.dataStore[wordKey];

        let length = wordsForWordKey.length;
        wordsForWordKey.slice(0).forEach(w => {
            this.deleteFromWordLengths(w);
            this.deleteWordFromNumberOfAnagramDict(wordKey, length);
            length--;
            this.saveDatastore();
        });
    }

    async getWordCount() {
        await this.loadDatastore();

        let numberOfWords = 0;
        let min = 1000;
        let max = 0;
        let totalWordLength = 0.0;
        let average = 0.0;
        let median = 0.0;

        const wordLengths = Object.keys(this.wordLengths);

        let allWords = [];

        for (let i = 0; i < wordLengths.length; i++) {
            let length = parseInt(wordLengths[i]);
            const count = this.wordLengths[wordLengths[i]];
            numberOfWords += this.wordLengths[length];
            if (length < min) min = length;
            if (length > max) max = length;
            totalWordLength += length * count;

            for (let j = 0; j < count; j++) {
                allWords.push(length);
            }
        }

        // Sort it to be safe
        allWords.sort((a, b) => parseInt(a) - parseInt(b));

        let index = (allWords.length - 1) / 2;

        if (index == Math.floor((allWords.length - 1) / 2)) {
            median = allWords[index];
        } else {
            median = (allWords[Math.floor(index)] + allWords[Math.ceil(index)]) / 2;
        }

        average = totalWordLength / numberOfWords;
        return { count: numberOfWords, min: min, max: max, average: average, median: median };
    }

    allAnagrams(words: string[]) {
        const wordKeys = words.map(word => this.getWordKey(word));
        return wordKeys.every(x => x === wordKeys[0]);
    }

    async getMostAnagrams() {
        await this.loadDatastore();

        const wordKeys = this.numberOfAnagrams.getMostAnagrams();

        const anagrams = [];
        wordKeys.forEach(wordKey => {
            const words = this.getWordsForWordKeys(wordKey, false);
            anagrams.push(words);
        });
        return { anagrams: anagrams };
    }

    async getAnagramsOfMinimunSize(minSize: number) {
        this.loadDatastore();

        const wordKeys = this.numberOfAnagrams.getAnagramsOfMinimunSize(minSize);

        const anagrams = [];
        wordKeys.forEach(wordKey => {
            const words = this.getWordsForWordKeys(wordKey, false);
            anagrams.push(words);
        });
        return { anagrams: anagrams };
    }

    private mountRoutes(): void {
        const router = express.Router();

        router.get('/', async (req, res) => {
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json({
                text: 'Anagrams API. Nothing on /',
            });
        });

        router.get('/anagrams/:query', async (req, res) => {
            const word = req.params.query.split('.')[0];
            const limit = req.query.limit;
            const properNouns = req.query.propernouns !== undefined ? req.query.propernouns == 'true' : false;

            const anagrams = await this.getAnagrams(word, !properNouns);

            if (!this.checkIfWordExistsInList(word, anagrams)) {
                res.setHeader('Content-Type', 'application/json');
                res.status(200).json({
                    anagrams: [], // Word doesn't exist in dictionary, even if it might have an anagram return []
                });
            } else {
                const list = this.removeWordFromArray(anagrams, word);
                const result = this.getLimitedItemsFromList(list, limit);

                res.setHeader('Content-Type', 'application/json');
                res.status(200).json({
                    anagrams: result,
                });
            }
        });

        router.post('/words.json', async (req, res) => {
            let words = req.body.words;
            this.addWords(words);

            res.setHeader('Content-Type', 'application/json');
            res.status(201).json(req.body);
        });

        router.delete('/words.json', async (req, res) => {
            let words = req.body.words;
            this.deleteAll();

            res.setHeader('content-type', 'application/json');
            res.status(204).json({});
        });

        router.delete('/words/:word.json', async (req, res) => {
            let word = req.params.word;

            this.deleteWord(word);

            res.setHeader('content-type', 'application/json');
            res.status(204).json({});
        });

        router.delete('/anagrams/:word.json', async (req, res) => {
            let word = req.params.word;
            this.deleteWordAndAnagrams(word);

            res.setHeader('content-type', 'application/json');
            res.status(204).json({});
        });

        router.get('/countWords/', async (req, res) => {
            // Is not case sensitive
            const result = await this.getWordCount();
            res.setHeader('Content-Type', 'application/json');
            console.log(result);
            console.log(this.dataStore);
            console.log(this.wordLengths);

            res.status(200).json(result);
        });

        router.get('/mostAnagrams/', async (req, res) => {
            // Is not case sensitive
            const result = await this.getMostAnagrams();
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json(result);
        });

        router.get('/anagramsOfSize/:minSize', async (req, res) => {
            // Is not case sensitive
            const result = await this.getAnagramsOfMinimunSize(parseInt(req.params.minSize));
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json(result);
        });

        router.post('/allAnagrams/', async (req, res) => {
            // Only checks if they are "anagrams, not if they are actually in the dictionary"
            let words = req.body.words;
            const result = this.allAnagrams(words);
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json({ all: result });
        });

        router.get('/loadData/', async (req, res) => {
            const result = await this.loadData();
            res.setHeader('Content-Type', 'application/json');
            res.status(201).json({ status: 'Loaded data' });
        });

        this.express.use('/', router);
    }
}
