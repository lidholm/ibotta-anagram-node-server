import * as supertest from 'supertest';
import { App, NumberOfAnagrams } from './App';
import { expect } from 'chai';

class TestApp extends App {
    setup() {}
}

xdescribe('App', () => {
    const app = new TestApp();
    it('works', () => {
        const a = 1;
        expect(a).to.equal(1);
    });

    it('wordKeys correctly', () => {
        expect(app.getWordKey('dear')).to.equal('ader');
        expect(app.getWordKey('read')).to.equal('ader');
        expect(app.getWordKey('abba')).to.equal('aabb');
    });

    it('remove duplicates from a list', () => {
        expect(app.unique(['a', 'b', 'c', 'b'])).to.eql(['a', 'b', 'c']);

        expect(app.unique(['a', 'a', 'a', 'b', 'c', 'b'])).to.eql(['a', 'b', 'c']);
    });

    it('remove word from array', () => {
        const array = ['aaa', 'bbb', 'ccc'];
        const arrayWithoutAAA = app.removeWordFromArray(array, 'aaa');
        expect(arrayWithoutAAA).to.eql(['bbb', 'ccc']);
        expect(array).to.eql(['aaa', 'bbb', 'ccc']);

        const arrayWithoutWordThatWasntInArray = app.removeWordFromArray(array, 'ddd');
        expect(arrayWithoutWordThatWasntInArray).to.eql(['aaa', 'bbb', 'ccc']);
        expect(array).to.eql(['aaa', 'bbb', 'ccc']);
    });
});

describe('NumberOfAnagrams', () => {
    let ana;

    beforeEach(() => {
        ana = new NumberOfAnagrams();
    });

    function checkArray(array, dict) {
        const keys = Object.keys(dict);
        keys.forEach(key => {
            console.log(key);
            console.log(dict[key]);
            console.log(Object.keys(array[key]));

            expect(Object.keys(array[key])).to.eql(dict[key]);
        });
    }

    it(' can add 2 of one wordKey, 2 of another wordKey, and 1 of yet another wordKey', () => {
        ana.addWord('ared', 1);
        ana.addWord('ared', 2);
        ana.addWord('art', 1);
        ana.addWord('aabb', 1);
        ana.addWord('art', 2);

        const expected = { 1: ['aabb'], 2: ['ared', 'art'] };
        checkArray(ana.lengths, expected);
    });

    it(' can add 2, then delete, then add again', () => {
        ana.addWord('ared', 1);
        ana.addWord('ared', 2);
        ana.addWord('art', 1);
        ana.addWord('art', 2);
        checkArray(ana.lengths, { 2: ['ared', 'art'] });
        expect(ana.getMostAnagrams()).to.eql(['ared', 'art']);

        ana.deleteWord('ared', 1);
        checkArray(ana.lengths, { 1: ['ared'], 2: ['art'] });
        expect(ana.getMostAnagrams()).to.eql(['art']);

        ana.addWord('ared', 2);
        checkArray(ana.lengths, { 2: ['art', 'ared'] });
        expect(ana.getMostAnagrams()).to.eql(['art', 'ared']);
    });

    it(' can add 2, then delete, then add again', () => {
        ana.addWord('ared', 1);
        ana.addWord('ared', 2);
        ana.addWord('ared', 3);
        ana.addWord('art', 1);
        ana.addWord('art', 2);
        ana.addWord('erd', 1);
        ana.addWord('erd', 2);
        ana.addWord('erd', 3);
        ana.addWord('abc', 1);

        // checkArray(ana.lengths, { 2: ['art', 'ared'] });
        expect(ana.getAnagramsOfMinimunSize(2)).to.eql(['ared', 'erd', 'art']);
        expect(ana.getAnagramsOfMinimunSize(3)).to.eql(['ared', 'erd']);
    });
});
